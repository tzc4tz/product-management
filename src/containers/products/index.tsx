import { ChangeEvent, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Form from "../../components/form";
import { FormProps } from "../../components/form/interface";
import Search from "../../components/search";
import Table from "../../components/table";
import constants from "../../utils/apiRoutes";
import useApi, { DataProps } from "../../utils/useApi";
import useDebounce from "../../utils/useDebounce";
import styles from "./products.module.scss";

const Products = () => {
  const { root, GET_PRODUCTS, GET_CATEGORIES, CREATE_PRODUCT } = constants;
  const params = useParams();
  const [value, setValue] = useState("");
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [newProduct, setNewProduct] = useState<FormProps & { id: string }>();
  const [searchedItem, setSearchItem] = useState<DataProps>();
  const debouncedValue = useDebounce(value);
  const { data, loading } = useApi(`${root}${GET_PRODUCTS}`);
  const { data: categories } = useApi(`${root}${GET_CATEGORIES}`);

  const handleSubmit = (formData: FormProps) => {
    const id = params?.["*"];
    const url = id
      ? `${root}${GET_PRODUCTS}/${id}`
      : `${root}${CREATE_PRODUCT}`;
    fetch(url, {
      method: id ? "PUT" : "POST",
      body: JSON.stringify(formData),
    })
      .then((res) => res.json())
      .then((response) => {
        setNewProduct({ ...formData, id: response?.id });
        setIsSubmitted(true);
      });
  };

  const searchProduct = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  useEffect(() => {
    // search api
    if (debouncedValue) {
      fetch(`${root}${GET_PRODUCTS}/search?q=${debouncedValue}`)
        .then((res) => res.json())
        .then((json) => setSearchItem(json));
    } else setSearchItem(undefined);
  }, [debouncedValue]);

  return (
    <div className={styles["products"]}>
      <Form
        categories={categories as string[]}
        onSubmit={handleSubmit}
        hasSubmitted={isSubmitted}
      />
      <div className={styles["products__list"]}>
        <div className={styles["products__list--header"]}>
          <span className={styles["products__list--title"]}>Products</span>
          <Search handleSearch={searchProduct} />
        </div>
        <Table
          data={data as DataProps}
          newProduct={newProduct!}
          searchedItem={searchedItem!}
          loading={loading}
        />
      </div>
    </div>
  );
};

export default Products;
