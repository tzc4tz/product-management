import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { TableProps } from "./interface";
import styles from "./table.module.scss";
import Skeleton from "../skeleton";

const Table = ({ data, newProduct, searchedItem, loading }: TableProps) => {
  const [pagination, setPagination] = useState([0, 5]);
  const navigate = useNavigate();

  const handlePagination = (type: "back" | "next") => {
    if (type === "next" && pagination[1] < data.limit) {
      setPagination((prev) => [prev[0] + 5, prev[1] + 5]);
      return;
    }
    if (type === "back" && pagination[0] !== 0) {
      setPagination((prev) => [prev[0] - 5, prev[1] - 5]);
    }
  };

  const selectProduct = (id: string | number) => {
    navigate(`/${id}`);
  };

  return (
    <div className={styles["table"]}>
      <div className={styles["table__header"]}>
        <span></span>
        <span>NAME</span>
        <span>CATEGORY</span>
        <span>BRAND</span>
        <span>PRICE</span>
        <span>DISCOUNT(%)</span>
      </div>
      {loading && <Skeleton />}
      {searchedItem ? (
        searchedItem?.products
          ?.slice(pagination[0], pagination[1])
          .map((item) => (
            <div
              onClick={() => selectProduct(item.id)}
              key={item.id}
              className={styles["table__data"]}
            >
              <img src={item.thumbnail} alt={item.title} />
              <span>{item.title}</span>
              <span>{item.category}</span>
              <span>{item.brand}</span>
              <span>{item.price} $</span>
              <span>{item.discountPercentage}%</span>
            </div>
          ))
      ) : (
        <>
          {newProduct && (
            <section data-type="product" data-product-id={newProduct.id}>
              <div
                onClick={() => selectProduct(newProduct.id)}
                className={styles["table__data"]}
              >
                <span className={styles["table__data--blank"]}></span>
                <span>{newProduct.title}</span>
                <span>{newProduct.category}</span>
                <span>{newProduct.brand}</span>
                <span>{newProduct.price} $</span>
                <span>{newProduct.discount}%</span>
              </div>
            </section>
          )}
          {data?.products?.slice(pagination[0], pagination[1]).map((item) => (
            <div
              onClick={() => selectProduct(item.id)}
              key={item.id}
              className={styles["table__data"]}
            >
              <img src={item.thumbnail} alt={item.title} />
              <span>{item.title}</span>
              <span>{item.category}</span>
              <span>{item.brand}</span>
              <span>{item.price} $</span>
              <span>{item.discountPercentage}%</span>
            </div>
          ))}
        </>
      )}

      <div className={styles["table__pagination"]}>
        <span className={styles["table__pagination--status"]}>
          {pagination[1] / 5} of 6
        </span>
        <div
          onClick={() => handlePagination("back")}
          className={styles["table__pagination--back"]}
        >
          <span />
        </div>
        <div
          onClick={() => handlePagination("next")}
          className={styles["table__pagination--next"]}
        >
          <span />
        </div>
      </div>
    </div>
  );
};

export default Table;
