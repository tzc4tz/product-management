import { DataProps } from "../../utils/useApi";
import { FormProps } from "../form/interface";

export type NewProductType = FormProps & { id: string };

export interface TableProps {
  data: DataProps;
  newProduct: NewProductType;
  searchedItem: DataProps;
  loading: boolean;
}
