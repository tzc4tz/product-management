import { useNavigate } from "react-router-dom";
import { avatar, logo } from "../../assets/images";
import styles from "./header.module.scss";

const Header = () => {
  const username = "Admin";
  const navigate = useNavigate();

  const handleClick = () => navigate("/");

  return (
    <div className={styles["header"]}>
      <img
        src={logo}
        alt="logo"
        onClick={handleClick}
        className={styles["header--logo"]}
      />
      <div className={styles["header__profile"]}>
        <img src={avatar} alt="profile avatar" />
        <span>{username}</span>
        <i className={styles["header__dropdown"]} />
      </div>
    </div>
  );
};

export default Header;
