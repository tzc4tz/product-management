import {
  AssessmentIcon,
  InventoryIcon,
  OfferIcon,
  SettingIcon,
  ProductsIcon,
} from "../../assets/icons";
import styles from "./sidebar.module.scss";

const Sidebar = () => {
  return (
    <div className={styles["sidebar"]}>
      <ProductsIcon className={styles["sidebar--item"]} />
      <OfferIcon className={styles["sidebar--item"]} />
      <InventoryIcon className={styles["sidebar--item"]} />
      <AssessmentIcon className={styles["sidebar--item"]} />
      <SettingIcon className={styles["sidebar--item"]} />
    </div>
  );
};

export default Sidebar;
