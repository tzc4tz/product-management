import styles from "./skeleton.module.scss";

const Skeleton = () => {
  return (
    <div className={styles["skeleton"]}>
      {[...Array(5)].map((_, index) => (
        <div key={index} className={styles["skeleton__wrapper"]}>
          <span className={styles["skeleton--image"]}></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      ))}
    </div>
  );
};

export default Skeleton;
