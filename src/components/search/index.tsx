import { ChangeEvent } from "react";
import { SearchIcon } from "../../assets/icons";
import styles from "./search.module.scss";

const Search = ({
  handleSearch,
}: {
  handleSearch: (e: ChangeEvent<HTMLInputElement>) => void;
}) => {
  return (
    <div className={styles["search"]}>
      <input placeholder="Search" onChange={handleSearch} />
      <SearchIcon className={styles["search--icon"]} />
    </div>
  );
};

export default Search;
