import styles from "./form.module.scss";
import FormItem from "./formItem";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import { FormProps, Props } from "./interface";
import { validations } from "../../utils/validations";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import constants from "../../utils/apiRoutes";

const Form = ({ categories, onSubmit, hasSubmitted }: Props) => {
  const params = useParams();
  const navigate = useNavigate();
  const { root, GET_PRODUCTS } = constants;
  const url = `${root}${GET_PRODUCTS}/${params?.["*"]}`;
  const [isEditing, setIsEditing] = useState(false);
  const {
    handleSubmit,
    reset,
    control,
    setValue,
    formState: { errors },
  } = useForm<FormProps>({
    defaultValues: {
      title: "",
      price: "",
      discount: "",
      category: "",
      brand: "",
      description: "",
    },
  });

  useEffect(() => {
    if (params?.["*"]) {
      fetch(url)
        .then((res) => res.json())
        .then((json) => {
          setIsEditing(true);
          const dataObj = Object.keys(json);
          dataObj.map((item) => {
            if (item === "discountPercentage") {
              setValue("discount", json[item as keyof FormProps]);
            }
            setValue(item as keyof FormProps, json[item as keyof FormProps]);
          });
        });
    }
  }, [params]);

  useEffect(() => {
    if (hasSubmitted) reset();
  }, [hasSubmitted]);

  const deleteProduct = () => {
    fetch(url, { method: "DELETE" })
      .then((res) => res.json())
      .then(() => {
        setIsEditing(false);
        reset();
        navigate("/");
      });
  };

  return (
    <div className={styles["form"]}>
      <span className={styles["form--title"]}>
        {isEditing ? "Edit" : "Create a New Product"}
      </span>
      <div className={styles["form__wrapper"]}>
        <form onSubmit={handleSubmit(onSubmit as SubmitHandler<FormProps>)}>
          <Controller
            name="title"
            rules={validations.ProductName}
            control={control}
            render={({ field }) => (
              <FormItem
                isError={!!errors.title}
                errorText={errors.title?.message}
                label="Product Name"
                field={field}
              />
            )}
          />
          <div className={styles["form__wrapper--row"]}>
            <Controller
              name="price"
              rules={validations.Price}
              control={control}
              render={({ field }) => (
                <FormItem
                  isError={!!errors.price}
                  errorText={errors.price?.message}
                  label="Price ($)"
                  field={field}
                  type="number"
                  className={styles["form__wrapper--price"]}
                />
              )}
            />
            <Controller
              name="discount"
              rules={validations.Discount}
              control={control}
              render={({ field }) => (
                <FormItem
                  isError={!!errors.discount}
                  errorText={errors.discount?.message}
                  label="Discount (%)"
                  field={field}
                  type="number"
                  className={styles["form__wrapper--discount"]}
                />
              )}
            />
          </div>
          <Controller
            name="category"
            control={control}
            render={({ field }) => (
              <FormItem
                isError={!!errors.category}
                errorText={errors.category?.message}
                label="Category"
                field={field}
                fieldType="select"
                options={categories}
              />
            )}
          />
          <Controller
            name="brand"
            control={control}
            render={({ field }) => (
              <FormItem
                isError={!!errors.brand}
                errorText={errors.brand?.message}
                label="Brand"
                field={field}
              />
            )}
          />
          <Controller
            name="description"
            control={control}
            render={({ field }) => (
              <FormItem
                isError={!!errors.description}
                errorText={errors.description?.message}
                label="Description"
                field={field}
                fieldType="textArea"
              />
            )}
          />
          <div className={styles["form__btn"]}>
            <button
              className={styles["form__btn--clear"]}
              type="button"
              onClick={() => {
                if (isEditing) {
                  setIsEditing(false);
                  reset();
                  navigate("/");
                } else reset();
              }}
            >
              {isEditing ? "Cancel" : "Clear"}
            </button>
            {isEditing && (
              <button
                className={styles["form__btn--delete"]}
                type="button"
                onClick={deleteProduct}
              >
                Delete
              </button>
            )}
            <button
              onClick={() => {
                setIsEditing(false);
                navigate("/");
              }}
              className={styles["form__btn--submit"]}
              type="submit"
            >
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Form;
