export interface Props {
  categories: string[];
  onSubmit: (formData: FormProps) => void;
  hasSubmitted: boolean;
}

export interface FormProps {
  title: string;
  price: string;
  discount: string;
  category: string;
  brand: string;
  description: string;
}

export interface FormItemProps<T> {
  field: T;
  isError: boolean;
  errorText?: string;
  label: string;
  type?: "number" | "text";
  className?: string;
  fieldType?: "input" | "select" | "textArea";
  options?: string[];
}
