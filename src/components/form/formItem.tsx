import styles from "./form.module.scss";
import { FormItemProps } from "./interface";

const FormItem = <T extends { value: string; name: string }>({
  field,
  isError,
  errorText,
  label,
  type = "text",
  className = "",
  fieldType = "input",
  options,
}: FormItemProps<T>) => {
  const capitalizeFirstLetter = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };
  return (
    <div className={`${styles["form__item"]} ${className}`}>
      <label className={styles["form__item--label"]} htmlFor={field.name}>
        {label}
      </label>
      {fieldType === "select" ? (
        <select {...field}>
          <option value="" disabled hidden></option>
          {options?.map((item) => (
            <option key={item} value={item}>
              {capitalizeFirstLetter(item).split("-").join(" ")}
            </option>
          ))}
        </select>
      ) : fieldType === "textArea" ? (
        <textarea {...field} rows={4} cols={4} maxLength={100} />
      ) : (
        <input {...field} id={field.name} type={type} />
      )}
      {isError && <span className={styles["form--error"]}>{errorText}</span>}
    </div>
  );
};

export default FormItem;
