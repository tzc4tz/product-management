import Header from "../components/header";
import Sidebar from "../components/sidebar";
import { LayoutProps } from "./interface";
import styles from "./layout.module.scss";

const Layout = ({ children }: LayoutProps) => {
  return (
    <div className={styles["layout"]}>
      <Header />
      <Sidebar />
      <div className={styles["layout__content--wrapper"]}>{children}</div>
    </div>
  );
};

export default Layout;
