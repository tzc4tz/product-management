import React from "react";
import Layout from "./layout";

const Products = React.lazy(() => import("./containers/products"));

export const routes = [
  {
    path: "/*",
    element: (
      <Layout>
        <Products />
      </Layout>
    ),
  },
];
