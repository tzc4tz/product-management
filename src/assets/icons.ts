import { ReactComponent as AssessmentIcon } from "./icons/assessment.svg";
import { ReactComponent as InventoryIcon } from "./icons/inventory.svg";
import { ReactComponent as OfferIcon } from "./icons/local_offer.svg";
import { ReactComponent as SettingIcon } from "./icons/settings.svg";
import { ReactComponent as ProductsIcon } from "./icons/space_dashboard.svg";
import { ReactComponent as SearchIcon } from "./icons/search.svg";

export {
  AssessmentIcon,
  InventoryIcon,
  OfferIcon,
  SettingIcon,
  ProductsIcon,
  SearchIcon,
};
