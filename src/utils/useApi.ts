import { useEffect, useState } from "react";

export interface DataProps {
  limit: number;
  products: {
    brand: string;
    category: string;
    description: string;
    discountPercentage: number;
    id: number;
    images: string[];
    price: number;
    rating: number;
    stock: number;
    thumbnail: string;
    title: string;
  }[];
  skip: number;
  total: number;
}

const useApi = (url: string) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<DataProps | string[] | null>(null);

  const handleError = (error: unknown) => {
    console.log(error);
  };

  const fetchApi = () => {
    fetch(url)
      .then((res) => res.json())
      .then((json) => {
        setLoading(false);
        setData(json);
      })
      .catch((err) => {
        setLoading(false);
        handleError(err);
      });
  };

  useEffect(() => {
    fetchApi();
  }, []);

  return { loading, data };
};

export default useApi;
