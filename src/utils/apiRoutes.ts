const constants = {
  root: "https://dummyjson.com/",
  GET_PRODUCTS: "products",
  CREATE_PRODUCT: "products/add",
  GET_CATEGORIES: "products/categories",
};

export default constants;
