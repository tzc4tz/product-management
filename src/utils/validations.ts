export const validations = {
  ProductName: {
    required: { value: true, message: "product name is required" },
  },
  Price: {
    pattern: {
      value: /^[0-9]+$/,
      message: "price should be a number",
    },
  },
  Discount: {
    max: { value: 100, message: "discount can't be more than 100" },
    min: { value: 1, message: "discount can't be less than 1" },
    pattern: {
      value: /^[0-9]+$/,
      message: "discount should be a number",
    },
  },
};
